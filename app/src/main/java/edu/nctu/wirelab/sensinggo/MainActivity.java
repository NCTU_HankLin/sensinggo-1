package edu.nctu.wirelab.sensinggo;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.nctu.wirelab.sensinggo.BroadcastReceiver.ScreenStateReceiver;
import edu.nctu.wirelab.sensinggo.Connect.HttpsConnection;
import edu.nctu.wirelab.sensinggo.File.FileMaker;
import edu.nctu.wirelab.sensinggo.File.JsonParser;
import edu.nctu.wirelab.sensinggo.File.OutCypher;
import edu.nctu.wirelab.sensinggo.Fragment.InfoFragment;
import edu.nctu.wirelab.sensinggo.Fragment.LoginFragment;
import edu.nctu.wirelab.sensinggo.Fragment.LoginUserFragment;
import edu.nctu.wirelab.sensinggo.Fragment.MapFragment;
import edu.nctu.wirelab.sensinggo.Fragment.SimUserFragment;
import edu.nctu.wirelab.sensinggo.Fragment.SocialFragment;
import edu.nctu.wirelab.sensinggo.Fragment.UserFragment;
import edu.nctu.wirelab.sensinggo.Measurement.Location;
import edu.nctu.wirelab.sensinggo.Record.TrafficSnapshot;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION = 1000;
    private final String TagName = "MainActivity";
    public static String VERSION = "v1.0.21_2018/08/27";
    public static String APPVERSION = "21";
    public static String SHANSERVER = "1";
    //Button quitButton;
    private CheckBox autoCheckBox;
    //MainContentFragment mMain = null;
    UserFragment mUser = null;
    InfoFragment mInfo = null;
    SocialFragment mSocial = null;
    SimUserFragment mSim = null;
    MapFragment mMap = null;
	private Fragment selectedFragment;
    public static LoginFragment mLogin = null;
    public static LoginUserFragment mLoginUser = null;

    private edu.nctu.wirelab.sensinggo.File.JsonParser JsonParser = new JsonParser();

    public static String logPath, configPath;
    public static String logPrefix, recordPrefix;

    public static long startServiceTime;

    public static boolean tempAutoUploadByMobile;

    public static int originalScreenOffTime = 15000; //ms

    //a delay control all the sense info delay, such as all cell info, location update, traffic throughput
    public static int flashInterval = 1000; //in ms

    public static Location lu;

    //the traffic monitor that monitors all apps data usage
    public static TrafficSnapshot latest, previous;

    //the cypher for signing the data that is used to upload to server
    private OutCypher mOutCypher;

    //public int removeLogFlag = 0;

    private ScreenStateReceiver mReceiver; // for screen monitor

    //private NotificationManager notificationManager = null; //notify 2017/1/17
    //final int notifyID = 1;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (mUser == null) {
                        mUser = new UserFragment();
                        mUser.SetContext(MainActivity.this);
                    }
					//hideToFragment(selectedFragment, mUser);
					//selectedFragment = mUser;
                    replaceToFragment(mUser);

                    return true;


                case R.id.navigation_info:
                    if (RunIntentService.runFlag) {
                        //removeLogFlag = 0;
                        if (mInfo == null) {
                            Log.d("0523", "info1");
                            mInfo = new InfoFragment();
                            mInfo.setJsonParser(JsonParser);
                        }
						//hideToFragment(selectedFragment, mInfo);
						//selectedFragment = mInfo;
                        replaceToFragment(mInfo);
                    }

                    return true;
                case R.id.navigation_login:
                    if(UserConfig.myUserName.compareTo("DefaultUser")==0){
                        if(mLogin==null){
                            mLogin = new LoginFragment();
                            mLogin.setJsonParser(JsonParser);
                        }
						//hideToFragment(selectedFragment, mLogin);
						//selectedFragment = mLogin;
                        replaceToFragment(mLogin);
                    }

                    else{ // the user has loginned
                        if(mLoginUser==null){
                            Log.d("0523", "loginUser1");
                            mLoginUser = new LoginUserFragment();
                            mLoginUser.setJsonParser(JsonParser);
                            loginSuccess();
                        }



                        if (!mLoginUser.isAdded()) {
                            loginSuccess();
                        }


                    }
                    return true;

                case R.id.navigation_social:
                    if (mSocial == null) {
                        mSocial = new SocialFragment();
                        socialInfo();
                    }

                    if (!mSocial.isAdded()) {
                        socialInfo();
                    }

                        /*if (selectedFragment != mSocial) {
                            socialInfo();
                        }*/

                    return true;

                case R.id.navigation_map:
                    if (mMap == null) {
                        mMap = new MapFragment();
                    }
                    replaceToFragment(mMap);
                    return true;

            }
            return false;
        }

    };


    @Override
    protected void onResume() {
        super.onResume();

        UserFragment.setRunningText("APP STOP");
        if (RunIntentService.errorFlag == true) {
            Toast.makeText(MainActivity.this, "SIM card Error", Toast.LENGTH_LONG)
                    .show();
        }
        else {
            if (checkGpsStatus() == true) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.System.canWrite(this)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // redundant? peiyu
                            initPermission();

                        }
                    } else {
                        showPermissionSettingMsg();
                    }
                } else {//version under 6.0
                    UserFragment.setRunningText("APP is Running");

                    startServices();
                }
            } else {
                showGPSClosedMsg();
                //  startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }

    }

    public void onDestroy() { // need check 01/25/18 peiyu
        //if running stop Services
        //if (RunIntentService.RunFlag)
        //StopServices();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
        super.onDestroy();
    }

    public boolean checkGpsStatus() {
        lu = new Location(MainActivity.this);
        if (!lu.isOpenGps()) {
//            ShowDialogMsg.showDialogLong("Please turn your gps on");
            lu = null;
            return false;
        }
        if (RunIntentService.runFlag) {
            lu.getGPS();
        }
        lu = null;
        return true;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);*/

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        // Add screen monitor
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenStateReceiver();
        registerReceiver(mReceiver, intentFilter);
        //--------------------------------------------

        initVar();
        //mainButton.setBackgroundResource(R.drawable.icon_svg_home);


        if(getIntent().getBooleanExtra("LOGOUT",false)){
            Intent intent = new Intent(MainActivity.this, RunIntentService.class);
            stopService(intent);
            RunIntentService.runFlag = false;
            finish();
        }

    }

    public void connectServer(String method, String path, String info){
        HttpsConnection httpsConnection = new HttpsConnection(MainActivity.this);
        httpsConnection.setJsonParser(JsonParser);
        httpsConnection.setActivity(MainActivity.this);
        httpsConnection.setMethod(method, info);
        httpsConnection.execute(path);
        Log.d("0612conn", info);
    }

    public void socialInfo(){
        Log.d("0612", "socialInfo, call server");
        String userInfo = "username=" + UserConfig.myUserName;
        connectServer("POST", "/getSimInfo", userInfo );
    }

    public void loginSuccess() { //After server responds login successfully msg, call getUserInfo api
        Log.d("0523", "loginSuccess()");
        String loginInfo = "username=" + UserConfig.myUserName;
        connectServer("POST", "/getUserInfo",loginInfo);
        Log.d("0523info",UserConfig.myUserName);
    }

    public void updateConfigMoney(){
        String moneyInfo = "username=" + UserConfig.myUserName;
        connectServer("POST", "/getMoney", moneyInfo);

    }

    public void transferSocial(){
        //hideToFragment(selectedFragment, mSocial);
        //selectedFragment = mSocial;
        Log.d("0807", "trnasfer social");
        replaceToFragment(mSocial);
    }

    public void transferLoginUser(){
        //hideToFragment(selectedFragment, mLoginUser);
        //selectedFragment = mLoginUser;
        Log.d("0807", "trnasfer login");
        if (mLoginUser == null) {
            mLoginUser = new LoginUserFragment();
        }

        replaceToFragment(mLoginUser);
    }

    public void transferSimUser(){

        if(mSim == null){
            mSim = new SimUserFragment();
        }
        //hideToFragment(selectedFragment, mSim);
        //selectedFragment = mSim;
        replaceToFragment(mSim);

    }

    public void replaceToFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.FragmentContent, fragment);
        transaction.commit();
    }


    public void hideToFragment(Fragment origin, Fragment to) {
        Log.d("0731hideto",origin.toString() + "----- " +  to.toString());
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        if (origin!=to) {
            if (!to.isAdded()) {
                Log.d("0731Added", "button1");
                transaction.hide(origin).add(R.id.FragmentContent, to).commit();
            } else {
                Log.d("0731Added", "button2");
                transaction.hide(origin).show(to).commit();
            }
        }
        else {
            Log.d("0731", "the same");
        }
    }

    public void initBfRun() {
		selectedFragment = mUser;
        FileMaker.fileFirstWrite = true;

        latest = null;
        previous = new TrafficSnapshot(this);


        startServiceTime = System.currentTimeMillis();

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        APPVERSION = valueOf(pInfo.versionCode);
        //Log.d("APPVSERSION",""+APPVERSION);
    }

    public void initVar() {
        logPath = new String("/data/data/" + getPackageName()) + "/logs/";
        configPath = "/data/data/" + getPackageName() + "/config";

        //----
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        APPVERSION = valueOf(pInfo.versionCode);
        ///Log.d("APPVSERSION",""+APPVERSION);



        if (UserConfig.loadConfigFrom(configPath) == false) {
            UserConfig.setUserName("DefaultUser");
        }

        else { // update total from database
            Log.d("0810", "update money");
            updateConfigMoney();
            //String userInfo = "username=" + UserConfig.myUserName;
            //connectServer("POST", "/getMoney", userInfo );
        }
        /*else {
            loginSuccess();
        }*/
        JsonParser.setAccount(UserConfig.myUserName);

        logPrefix = new String("NCTU");
        recordPrefix = new String(UserConfig.myUserName + "RECORD");

        mOutCypher = new OutCypher();
        ShowDialogMsg.mcontext = getApplicationContext();

        if (mUser == null) {
            mUser = new UserFragment();
            mUser.SetContext(MainActivity.this);
        }
        replaceToFragment(mUser);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initPermission() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION)
                || !addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            permissionsNeeded.add("LOCATION");
        }

        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)
                || !addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("STORAGE");
        }

        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE)) {
            permissionsNeeded.add("PHONE");
        }

        if (!addPermission(permissionsList, Manifest.permission.CAMERA)) {
            permissionsNeeded.add("CAMERA");
        }

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_PERMISSION);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_PERMISSION);
            return;
        }
        UserFragment.setRunningText("APP is running");
        startServices();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);

            /**
             * This case means either 1 or 2:
             * 1. The user turned down the permission request in the past and
             *      choose the Don't ask again option in the permission request system dialog
             * 2. A device policy prohibits the app from having that permission
             *
             * Ref: https://developer.android.com/training/permissions/requesting.html
             */
            if (!shouldShowRequestPermissionRationale(permission)) {
                return false;
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ShowDialogMsg.showDialog("APP can't start without Permission");
                    }
                })
                .create()
                .show();
    }

    public void startServices() {
        if (RunIntentService.runFlag == false) {
            initBfRun();
            try {
                originalScreenOffTime = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            //Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 86400000 ); //86400000ms = 24hr

            RunIntentService.runFlag = true;
            RunIntentService.stopServiceFlag = false;
            RunIntentService.firstRoundFlag = true;
            Intent Intent = new Intent(MainActivity.this, RunIntentService.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("JsonParser", JsonParser);
            bundle.putSerializable("moutcypher", mOutCypher);
            Intent.putExtras(bundle);
            startService(Intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.action_camera) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, AugmentedRealityOri.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_multiple_choice);
            adapter.add("auto-upload by 3G/4G");

            tempAutoUploadByMobile = UserConfig.autoUploadByMobile;

            final View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.listview_social, null);
            ListView listview = (ListView) view.findViewById(R.id.list);
            listview.setAdapter(adapter);
            listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listview.setItemChecked(0, tempAutoUploadByMobile);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    SparseBooleanArray tempchoice;
                    AbsListView list = (AbsListView) adapterView;
                    //Log.d(TagName, "onItemClick");
                    tempchoice = list.getCheckedItemPositions();
                    for (int i = 0; i < tempchoice.size(); i++) {
                        int key = tempchoice.keyAt(i);
                        switch (key) {
                            case 0:
                                if (tempchoice.get(i)) tempAutoUploadByMobile = true;
                                else tempAutoUploadByMobile = false;
//                                Log.d(TagName, "tempAutoUploadByMobile: "+String.valueOf(TempAutoUploadByMobile));
                                break;
                        }
                    }
                }
            });

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Setting")
                    .setView(view)
                    .setCancelable(false)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            ShowDialogMsg.showDialog("Cancel");
                        }
                    })
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //UserConfig.autoUploadByMobile = tempAutoUploadByMobile;
                            UserConfig.setAutoUploadByMobile(tempAutoUploadByMobile);
//                            Log.d(TagName, "AutoUploadByMobile: "+String.valueOf(UserConfig.AutoUploadByMobile));
                            UserConfig.saveConfigTo(configPath);
                        }
                    })
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    break;
                }
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_DENIED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    startServices();
                    UserFragment.setRunningText("APP is Running");
                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Need opening permission", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showGPSClosedMsg() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle("GPS Checker")
                .setMessage("Please Turn Your GPS Service on")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).show();
    }

    public void showPermissionSettingMsg() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle("System setting Checker")
                .setMessage("Please Grant System Setting")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setData(Uri.parse("package:" + MainActivity.this.getPackageName()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }).show();
    }
}

