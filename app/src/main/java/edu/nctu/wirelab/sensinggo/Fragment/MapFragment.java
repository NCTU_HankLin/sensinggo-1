package edu.nctu.wirelab.sensinggo.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.location.Location;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
//import edu.nctu.wirelab.measuring.Measurement.Location;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.nctu.wirelab.sensinggo.AugmentedRealityOri;


import edu.nctu.wirelab.sensinggo.MainActivity;
import edu.nctu.wirelab.sensinggo.R;
import edu.nctu.wirelab.sensinggo.UserConfig;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements LocationListener {
    private MapView mMapView;
    private GoogleMap googleMap;
    private boolean buttonbool = true;
    private boolean prebutttonbool = true;
    private boolean loaddata = false;
    private Double recentlat = 24.787574;
    private Double recentlng = 120.997602;
    private Double recentresolution = 0.0;
    Button changeButton;
    LocationManager lm;
    private final static String getURL = "http://140.113.216.39:7999/putGPS/";

    ArrayList<Spot> spotlist = new ArrayList<Spot>();
    ArrayList<String> takencoinlist = new ArrayList<String>();
    ArrayList<SignalPoint> signalpointlist = new ArrayList<SignalPoint>();
    ArrayList<Double> latlist = new ArrayList<Double>();
    ArrayList<Double> lnglist = new ArrayList<Double>();
    ArrayList<Double> powerlist = new ArrayList<Double>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_google_maps, null, false);
        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        changeButton = (Button) view.findViewById(R.id.changeButton);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                double lat, lng;
                googleMap = mMap;
                UiSettings uisettings = googleMap.getUiSettings();
                uisettings.setMyLocationButtonEnabled(true);
                googleMap.setMyLocationEnabled(true);
                googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        final Marker temp_marker = marker;

                        new AlertDialog.Builder(getActivity())
                                .setMessage("Are you ready to obtain coin?")
                                .setIcon(R.mipmap.ic_launcher)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        temp_marker.setVisible(false);
                                        takencoinlist.add(temp_marker.getTitle());

                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                })
                                .show();
                        return true;

                    }
                });

                Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER); // 設定定位資訊由 GPS提供
                if(location == null){
                    Log.d("null", "is is null");
                    new AlertDialog.Builder(getActivity())
                            .setMessage("There is no GPS connection.")
                            .setIcon(R.mipmap.ic_launcher)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .show();
                }
                else {
                    lat = location.getLatitude();  // 取得經度
                    lng = location.getLongitude(); // 取得緯度
                    recentlat = location.getLatitude();
                    recentlng = location.getLongitude();
                    LatLng sydney = new LatLng(lat, lng);

                    googleMap.addCircle(new CircleOptions()
                            .center(sydney)
                            .radius(150)
                            .strokeColor(Color.RED));

                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(16).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
        });

        changeButton.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                googleMap.clear();
                if(buttonbool) buttonbool = false;
                else buttonbool = true;
                new Thread(new Runnable(){
                    @Override
                    public void run(){
                        latlist.clear();
                        lnglist.clear();
                        powerlist.clear();
                        Post_Message();

                        //new FetchDataTask().execute(getURL);

                    }
                }).start();
                Log.d("082222", "value " + latlist.size());
                onResume();
            }
        });

        //spotlist = new ArrayList<Spot>();
        spotlist.add(new Spot(24.7872268, 120.9969887, "coin1")); //小木屋鬆餅
//        if(loaddata == false){
//            new FetchDataTask().execute(getURL);
//            loaddata = true;
//        }

        return view;
    }

//    private class FetchDataTask extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params){
//
//            InputStream inputStream = null;
//            String result = null;
//            HttpClient client = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet(params[0]);
//
//            try {
//
//                HttpResponse response = client.execute(httpGet);
//                inputStream = response.getEntity().getContent();
//
//                // convert inputstream to string
//                if(inputStream != null){
//                    result = convertInputStreamToString(inputStream);
//                    Log.i("App", "Data received:" +result);
//
//                }
//                else
//                    result = "Failed to fetch data";
//
//                return result;
//
//            } catch (ClientProtocolException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String dataFetched) {
//            //parse the JSON data and then display
//
//            parseJSON(dataFetched);
//        }
//
//        private String convertInputStreamToString(InputStream inputStream) throws IOException{
//            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
//            String line = "";
//            String result = "";
//            while((line = bufferedReader.readLine()) != null)
//                result += line;
//
//            inputStream.close();
//            return result;
//
//        }
//
//        private void parseJSON(String data){
//
//            try{
//
//                JSONArray jsonMainNode = new JSONArray(data);
//                int jsonArrLength = jsonMainNode.length();
//
//                for(int i=0; i < jsonArrLength; i++) {
//                    Log.d("check", "check");
//                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
//
//                    latlist.add( jsonChildNode.getJSONObject("fields").getDouble("lat") );
//                    lnglist.add( jsonChildNode.getJSONObject("fields").getDouble("lng") );
//                    powerlist.add( jsonChildNode.getJSONObject("fields").getDouble("degree") );
//
//                }
//
//
//            }catch(Exception e){
//                Log.i("App", "Error parsing data" +e.getMessage());
//
//            }
//        }
//    }

    private void showCoin(Location location){
        for (int i = 0; i < spotlist.size(); i++) {
            Log.d("0815", "0815");
            Spot spot = spotlist.get(i);
            if(takencoinlist.contains(spot.name)) continue;

            Location spotloc = new Location(location);
            spotloc.setLatitude(spot.latitude);
            spotloc.setLongitude(spot.longitude);
            float dist = location.distanceTo(spotloc);

            LatLng smallwood = new LatLng(spot.latitude, spot.longitude);  // target location
            MarkerOptions mo = new MarkerOptions();
            mo.position(smallwood);
            mo.title(spot.name);
            int height = 50;
            int width = 50;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.icon_dollar);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
            mo.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

            Marker markername = googleMap.addMarker(mo);
            if (dist > 150.0) {
                markername.setVisible(false);
                continue;
            }
            else{
                markername.setVisible(true);
            }


        }
    }

    private void parseJSON(String data){

        try{

            JSONArray jsonMainNode = new JSONArray(data);
            int jsonArrLength = jsonMainNode.length();
            //Log.d("check", "check" + jsonArrLength);
            for(int i=0; i < jsonArrLength; i++) {
                //Log.d("check", "check" );
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                latlist.add( jsonChildNode.getJSONObject("fields").getDouble("lat") );
                lnglist.add( jsonChildNode.getJSONObject("fields").getDouble("lng") );
                powerlist.add( jsonChildNode.getJSONObject("fields").getDouble("degree") );

            }


        }catch(Exception e){
            Log.i("App", "Error parsing data" +e.getMessage());

        }
    }

    private void Post_Message(){
        String urlstring = "http://140.113.216.39:7999/putGPS/";
        //String urlstring = "http://wirelab.nctucs.net:8000/hello/welcome_post_json/";

        HttpURLConnection connection = null;
        try{
            //initial connection
            URL url = new URL(urlstring);
            //get connection object
            connection = (HttpURLConnection) url.openConnection();
            //set request
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("charset","utf-8");
            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(100000);


            JSONObject JsonObj = new JSONObject();
            Log.d("XDDDD", "value"+recentlat);
            JsonObj.put("lat", recentlat);
            JsonObj.put("lng", recentlng);
            JsonObj.put("resolution", recentresolution);
            JSONArray Json_send = new JSONArray();
            Json_send.put(JsonObj);


            DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
            dos.writeBytes(Json_send.toString());
            dos.flush();
            dos.close();
            System.out.println(Json_send.toString());
            int responseCode = connection.getResponseCode();
            Log.d("response", "Post_Message: " + responseCode);
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            String result = "";
            while((line = br.readLine()) != null)
                result += line;
            parseJSON(result);


            br.close();

            //System.out.println("WEB return value is : " + sb);
            Log.d("XXXXXXXXXXXXX", "Post_Message: " + result);
            // Toast.makeText(getApplicationContext(),"Sending 'POST' request to URL : " + url + "\nPost parameters : " + test + "\nResponse Code : " + responseCode + "\nWEB return value is : " + sb, Toast.LENGTH_LONG).show();
        }
        catch(IOException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    class Spot {
        Spot(double latitude, double longitude, String name) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.name = name;
        }
        double latitude;
        double longitude;
        String name;
    }

    class SignalPoint{
        SignalPoint(double latitude, double longitude, double power) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.power = power;
        }
        double latitude;
        double longitude;
        double power;
    }

    @Override
    public void onResume() {
        super.onResume();

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


        mMapView.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        lm.removeUpdates(this);
        mMapView.onPause();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void onLocationChanged(Location location) {
        if (location == null) return;
        recentlat = location.getLatitude();
        recentlng = location.getLongitude();

//        if(loaddata == false){
//            new FetchDataTask().execute(URL);
//            loaddata = true;
//        }

        LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());

        if(buttonbool) {
            googleMap.clear();
            Log.d("QQ", "this is true");
            googleMap.addCircle(new CircleOptions()
                    .center(sydney)
                    .radius(150)
                    .strokeColor(Color.RED));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(16).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            showCoin(location);
        }
        else{

            Log.d("QQ", "this is false");

            if(prebutttonbool == true) {
                googleMap.clear();
                int num = 0;
                for (int i = 0; i < powerlist.size(); i++) {

//                    Location spotloc = new Location(location);
//                    spotloc.setLatitude(latlist.get(i));
//                    spotloc.setLongitude(lnglist.get(i));
//                    float dist = location.distanceTo(spotloc);
//                    if(dist > 300) continue;
//                    if(num > 500) break;

                    Log.d("crazy", "hello" + num);
                    num++;
//                    Log.d("latlist", "hello " + latlist.get(i));
//                    Log.d("powerlist", "hello " + powerlist.get(i));
                    LatLng sigcor = new LatLng(latlist.get(i), lnglist.get(i));
                    CircleOptions circle = new CircleOptions();
                    circle.center(sigcor);
                    circle.radius(20);
                    circle.strokeWidth(0);
                    if(powerlist.get(i) == 0) {
                        //circle.strokeColor(0xffff0000);
                        circle.fillColor(0xffff0000);
                    }
                    else if(powerlist.get(i) == 1.0){
                        //circle.strokeColor(0x33ff0000);
                        circle.fillColor(0xffff0000);
                    }
                    else if(powerlist.get(i) == 2.0){
                        //circle.strokeColor(0x33ff0000);
                        circle.fillColor(0xffff0000);
                    }
                    else if(powerlist.get(i) == 3.0){
                        //circle.strokeColor(0x33ff0000);
                        circle.fillColor(0xffffff00);
                    }
                    else if(powerlist.get(i) == 4.0){
                        //circle.strokeColor(0x33ff0000);
                        circle.fillColor(0xff00ff00);
                    }
                    else if(powerlist.get(i) == 5.0){
                        //circle.strokeColor(0x33ff0000);
                        circle.fillColor(0xff00ff00);
                    }

                    googleMap.addCircle(circle);
                }


                googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(16).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
        prebutttonbool = buttonbool;
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider,
                                int status, Bundle extras) {
    }





}
